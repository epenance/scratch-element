# Scratch Element
Scratch element is a custom web component that you can use within your application. 
It provides a scratch card functionality, where all content within the tag will be covered until scratched.
It provides you with several options to customize the card exactly for your use.

## Installation
First copy the script tag into your website, then wrap any html you want covered in the scratch-element tag 

```html
<script type="application/javascript" src="/js/scratch-element.min.js"></script>

<scratch-element>
    <h1>My content</h1>
    <p>Will be hidden until scratched</p>
</scratch-element>
```

## Config
The scratch element comes with several options you can apply to customize it to fit your use-case.

* The `coverImage (default: assets/background.png)` property gives you the opportunity to change the image used to cover your content. 
* The `brushImage (default: assets/brush.png)` property gives you the opportunity to change the image used as the brush pattern. 
* The `winPercentage (default: 75)` property gives you the opportunity to change when the image should be fully revealed and trigger the onComplete event. 
* The `backgroundMode (default: cover)` property gives you the opportunity to change how the cover image should be covering your content. Currently we support 2 modes: `cover` and `repeat`

Use them by simply applying the property to your html element

```html
<scratch-element coverImage="https://picsum.photos/200/180" backgroundMode="repeat" winPercentage="80">
    <h1>My content</h1>
    <p>Will be hidden until scratched</p>
</scratch-element>
```  
