const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');

const prod = require('./webpack.prod.js');

module.exports = merge(prod, {
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Scratch Element',
            template: './src/index.html',
            chunks: ['scratch-element.min']
        }),
    ],
    output: {
        path: path.resolve(__dirname, '../public')
    }
});
