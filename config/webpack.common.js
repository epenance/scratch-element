const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
    entry: './src/index.ts',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css|\.s(c|a)ss$/,
                exclude: /main.scss/,
                use: [
                    {
                        loader: 'lit-scss-loader',
                        options: {
                            minify: process.env.NODE_ENV === 'production', // defaults to false
                        },
                    },
                    'extract-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: ['./src/styles/main.scss']
                        }
                    },
                    {
                        loader: 'sass-resources-loader',
                        options: {
                            resources: ['./src/styles/variables/*.scss']
                        }
                    },
                ],
            },
        ]
    },
    resolve: {
        plugins: [new TsconfigPathsPlugin()],
        extensions: [ '.tsx', '.ts', '.js' ]
    },
    plugins: [
        // new CleanWebpackPlugin(['dist/*']) for < v2 versions of CleanWebpackPlugin
        new CleanWebpackPlugin(),
        new CopyPlugin([
            { from: './src/assets/', to: './assets'},
        ], {
            copyUnmodified: true
        }),
    ],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, '../dist')
    }
};
