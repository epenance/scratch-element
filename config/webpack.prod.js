const webpack = require('webpack');
const merge = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'production',
    entry: {
        'scratch-element': './src/index.ts',
        'scratch-element.min': './src/index.ts',
    },
    stats: {
        colors: false,
        hash: true,
        timings: true,
        assets: true,
        chunks: true,
        chunkModules: true,
        modules: true,
        children: true,
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                sourceMap: true,
                include: /\.min\.js$/,
                extractComments: false,
                terserOptions: {
                    output: {
                        comments: false,
                    }
                },
            })
        ],
        runtimeChunk: false,
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            },
        }),
        new CopyPlugin([
            { from: './README.md', to: './'},
            { from: './LICENSE', to: './'},
        ], {
            copyUnmodified: true
        }),
    ],
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, '../dist')
    }
});
