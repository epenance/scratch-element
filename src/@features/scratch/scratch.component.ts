import {customElement, html, LitElement, property, TemplateResult} from "lit-element";
import {background, brush} from "@features/scratch/scratch.constants";

import style from './scratch.component.scss';

@customElement('scratch-element')
export default class ScratchComponent extends LitElement {
    @property({type: String}) coverImage = background;
    @property({type: String}) brushImage = brush;
    @property({type: Number}) winPercentage = 75;
    @property({type: String}) backgroundMode = 'cover';

    isDrawing = false;
    lastPoint;

    container: HTMLElement;
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;

    canvasWidth: number = 0;
    canvasHeight: number = 0;

    image = new Image();
    brush = new Image();

    complete = false;
    init = false;


    constructor() {
        super();
    }

    initCanvas() {
        this.container = this.shadowRoot.getElementById('scratch-wrapper');
        this.canvas = <HTMLCanvasElement> this.shadowRoot.getElementById('scratch-canvas');

        this.canvasWidth = this.container.offsetWidth;
        this.canvasHeight = this.container.offsetHeight;

        this.canvas.addEventListener('mousedown', this.handleMouseDown.bind(this), false);
        this.canvas.addEventListener('touchstart', this.handleMouseDown.bind(this), false);
        this.canvas.addEventListener('mouseup', this.handleMouseUp.bind(this), false);
        this.canvas.addEventListener('touchend', this.handleMouseUp.bind(this), false);
        this.canvas.addEventListener('mousemove', this.handleMouseMove.bind(this), false);
        this.canvas.addEventListener('touchmove', this.handleMouseMove.bind(this), false);

        this.image.onload = () => {
            switch (this.backgroundMode) {
                case 'repeat':
                    this.requestUpdate().then(() => this.drawScratchRepeat());
                    break;
                case 'cover':
                    this.requestUpdate().then(() => this.drawScratchCover());
                    break;
                default:
                    this.requestUpdate().then(() => this.drawScratchCover());
            }

            this.init = true;
            this.requestUpdate();

            const event = new CustomEvent('scratchInit', {
                detail: {
                    message: 'Scratch card has been rendered'
                },
                bubbles: true,
                composed: true
            });

            this.dispatchEvent(event);
        };

        this.image.crossOrigin = 'Anonymous';
        this.image.src = this.coverImage;

        this.brush.crossOrigin = 'Anonymous';
        this.brush.src = brush;

        this.ctx = this.canvas.getContext('2d');
    }

    static get styles() {
        return style;
    }

    drawScratchRepeat() {
        this.ctx.fillStyle = this.ctx.createPattern(this.image, 'repeat');

        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    }

    drawScratchCover() {

        const x = 0;
        const y = 0;
        const width = this.ctx.canvas.width;
        const height = this.ctx.canvas.height;

        const offsetX = 0.5;
        const offsetY = 0.5;

        const imageWidth = this.image.width;
        const imageHeight = this.image.height;

        const r = Math.min(width / imageWidth, height / imageHeight);

        let newWidth = imageWidth * r;
        let newHeight = imageHeight * r;

        let cx, cy, cw, ch, ar = 1;

        if (newWidth < width) ar = width / newWidth;
        if (Math.abs(ar - 1) < 1e-14 && newHeight < height) ar = height / newHeight;

        newWidth *= ar;
        newHeight *= ar;

        // calc source rectangle
        cw = imageWidth / (newWidth / width);
        ch = imageHeight / (newHeight / height);

        cx = (imageWidth - cw) * offsetX;
        cy = (imageHeight - ch) * offsetY;

        // make sure source rectangle is valid
        if (cx < 0) cx = 0;
        if (cy < 0) cy = 0;
        if (cw > imageWidth) cw = imageWidth;
        if (ch > imageHeight) ch = imageHeight;


        this.ctx.drawImage(this.image, cx, cy, cw, ch,  x, y, width, height);
    }

    handleMouseDown(event) {
        this.isDrawing = true;
        this.lastPoint = this.getMouse(event);
    }

    handleMouseUp(event) {
        this.isDrawing = false;
    }

    handleMouseMove(event) {
        if (!this.isDrawing) return;

        event.preventDefault();

        const currentPoint = this.getMouse(event);
        const dist = this.distanceBetween(currentPoint);
        const angle = this.angleBetween(currentPoint);

        let x, y;

        for (let i = 0; i < dist; i++) {
            x = this.lastPoint.x + (Math.sin(angle) * i) - 25;
            y = this.lastPoint.y + (Math.cos(angle) * i) - 25;
            this.ctx.globalCompositeOperation = 'destination-out';
            this.ctx.drawImage(this.brush, x, y);
        }

        this.lastPoint = currentPoint;

        this.handlePercentage(this.getFilledInPixels(32));
    }

    handlePercentage(filledInPixels) {
        filledInPixels = filledInPixels || 0;
        if (filledInPixels > this.winPercentage) {

            const event = new CustomEvent('scratchComplete', {
                detail: {
                    amount: filledInPixels
                },
                bubbles: true,
                composed: true
            });

            this.dispatchEvent(event);

            this.complete = true;

            this.canvas.parentNode.removeChild(this.canvas);

            this.requestUpdate();
        }
    }

    getFilledInPixels(stride) {
        if (!stride || stride < 1) { stride = 1; }

        let pixels   = this.ctx.getImageData(0, 0, this.canvasWidth, this.canvasHeight),
            pdata: any    = pixels.data,
            l        = pdata.length,
            total    = (l / stride),
            count    = 0;

        // Iterate over all pixels
        for(let i = count = 0; i < l; i += stride) {
            if (parseInt(pdata[i]) === 0) {
                count++;
            }
        }

        return Math.round((count / total) * 100);
    }

    distanceBetween(currentPoint) {
        return Math.sqrt(Math.pow(currentPoint.x - this.lastPoint.x, 2) + Math.pow(currentPoint.y - this.lastPoint.y, 2));
    }

    angleBetween(currentPoint) {
        return Math.atan2( currentPoint.x - this.lastPoint.x, currentPoint.y - this.lastPoint.y );
    }

    getMouse(event) {
        let offsetX = 0, offsetY = 0, mx, my;

        if(this.canvas.offsetParent !== undefined) {
            do {
                offsetX += this.canvas.offsetLeft;
                offsetY += this.canvas.offsetTop;
            } while((this.canvas === this.canvas.offsetParent));
        }

        mx = (event.pageX || event.touches[0].clientX) - offsetX;
        my = (event.pageY || event.touches[0].clientY) - offsetY;

        return {
            x: mx, y: my
        };
    }

    getClass() {
        let classes = [];

        if (this.complete) classes.push('complete');

        if (this.init) classes.push('active');

        return classes.join(' ');
    }

    protected firstUpdated(_changedProperties: Map<PropertyKey, unknown>): void {
        this.initCanvas();
    }

    protected render(): TemplateResult | void {
        return html`
                    <div id="scratch-wrapper">
                        <canvas id="scratch-canvas" width="${this.canvasWidth}" height="${this.canvasHeight}"></canvas>
                        <div id="scratch-content" class=${this.getClass()}>
                            <slot></slot>
                        </div>
                    </div>
                    `;
    }
}
