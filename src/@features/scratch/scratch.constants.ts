const currentScript = document.currentScript as HTMLScriptElement;
const path = currentScript.src.split('?')[0];
const dir = path.split('/').slice(0, -1).join('/')+'/';

export const brush = dir + 'assets/brush.png';

export const background = dir + 'assets/background.png';
